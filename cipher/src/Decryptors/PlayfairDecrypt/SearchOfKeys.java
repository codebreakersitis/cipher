package Decryptors.PlayfairDecrypt;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anna
 * 11-602
 */

public class SearchOfKeys {
    public static List<String> length3 = new ArrayList<>();
    public static List<String> length2 = new ArrayList<>();
    static {
        StringBuilder stringBuilder = new StringBuilder();
        for (char i = 'А'; i <= 'Я'; i++) {
            for (char j = 'А'; j <= 'Я'; j++) {
                for (char k = 'А'; k <= 'Я'; k++) {
                    stringBuilder.append(i);
                    stringBuilder.append(j);
                    stringBuilder.append(k);
                    length3.add(stringBuilder.toString());
                    stringBuilder.delete(0, stringBuilder.length());
                }

            }
        }
        for (char i = 'А'; i <= 'Я'; i++) {
            for (char j = 'А'; j <= 'Я'; j++) {
                stringBuilder.append(i);
                stringBuilder.append(j);
                length2.add(stringBuilder.toString());
                stringBuilder.delete(0, stringBuilder.length());
            }
        }
    }
}
