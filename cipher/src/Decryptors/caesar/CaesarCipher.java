package Decryptors.caesar;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;

import Dictionary.Dictionary;

/**
 * @author Albina
 * 11-602
 */

public class CaesarCipher {
    public static void main(String[] args) throws Exception {
        FileChannel fc = new FileInputStream("src/Decryptors/caesar/input.txt").getChannel();
        FileChannel out = new FileOutputStream("src/Decryptors/caesar/output.txt").getChannel();
        CharBuffer charBuffer = CharBuffer.allocate((int) fc.size() * 100);
        ByteBuffer byteBuffer = ByteBuffer.allocate((int) fc.size() * 100);
        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

        fc.read(byteBuffer);
        byteBuffer.flip();
        charBuffer.clear();
        charBuffer = decoder.decode(byteBuffer);

        boolean isFound = decrypte(charBuffer);

        if (isFound)
            out.write(encoder.encode(charBuffer));
        else System.out.println("I can't decrypt this text :(");

        fc.close();
    }

    private static boolean decrypte(CharBuffer charBuffer) {
        int k = 1;
        boolean isFound = false;

        while (k < 32) {
            for (int i = 0; i < charBuffer.length(); i++) {
                char s = Character.toLowerCase(charBuffer.get(i));
                if (checkChar(s)) {
                    int new_s = s + 1;
                    if (new_s > 'я') {
                        new_s -= 32;
                    }
                    charBuffer.put(i, (char) new_s);
                }
            }

            char[] s = charBuffer.array();
            List<String> words = new ArrayList<>();
            String word = "";
            for (int j = 0; j < s.length; j++) {
                if (s[j] == ' ') {
                    words.add(word);
                    word = "";
                } else if (checkChar(s[j])) {
                    word += s[j];
                }
            }

            int m = 0;
            for (String w : words) {
                if(Dictionary.exists(w)) {
                    m++;
                }
            }

            if (m >= words.size() / 2) {
                isFound = true;
                break;
            }

            k++;
        }
        return isFound;
    }

    private static boolean checkChar(char s) {
        return s != ' ' && s != '.' && s != ',' && s != '?' && s != '!' && s != '—' && s != ';' && s != ':' &&
                s != '«' && s != '»';
    }
}