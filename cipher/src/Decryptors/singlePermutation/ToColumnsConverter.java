package Decryptors.singlePermutation;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anna
 * 11-602
 */

public class ToColumnsConverter {
    public static List<Character>[] getColumns(String file, int numberOfColumns) throws Exception{
        FileChannel in = new FileInputStream(file).getChannel();
        //FileChannel out = new FileInputStream("outputTest.txt").getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate((int)in.size());
        in.read(byteBuffer);
        byteBuffer.rewind();

        CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);

        List[] columns = new List[numberOfColumns];
        for (int i = 0; i < numberOfColumns; i++) {
            columns[i] = new ArrayList();
        }

        int n = charBuffer.length();
        int j = 0;

        for (int i = 0; i < n; i++) {
            columns[j % numberOfColumns].add(charBuffer.get());
            j++;
        }
        return columns;
    }
}
