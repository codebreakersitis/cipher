package Decryptors.singlePermutation;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author Anna
 * 11-602
 */

public class Output {
    public static void write(List<Character> list) throws Exception{
        CharBuffer charBuffer = CharBuffer.allocate(list.size());
        for (int i = 0; i < list.size(); i++) {
            charBuffer.put(i, list.get(i));
        }
        ByteBuffer byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
        FileChannel fc = new FileOutputStream("src/Decryptors/singlePermutation/outputTest2.txt").getChannel();
        fc.write(byteBuffer);
    }
}
