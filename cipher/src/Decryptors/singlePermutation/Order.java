package Decryptors.singlePermutation;

import java.util.ArrayList;
import java.util.List;

import Dictionary.Dictionary;

/**
 * @author Anna
 * 11-602
 */

public class Order {
    //для небольшого количества элементов перестановки проще задать вручную, чем программой:)
    private static int[][] permutation3 = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 1, 0}, {2, 0, 1}};
    private static int[][] permutation2 = {{0, 1}, {1, 0}};

    public static List<Character> determineTheOrder(List<Character>[] columns) {
        int length = columns.length;
        List<Character> text = new ArrayList<>();
        int[][] permutation;
        if (length == 3) {
            permutation = permutation3;
        }
        else permutation = permutation2;
        boolean flag = false;

        for (int i = 0; i < permutation.length && !flag; i++) {
            for (int j = 0; j < permutation[0].length; j++) {
                text.addAll(columns[permutation[i][j]]);
            }

            //text.forEach(s -> System.out.print(s));
            //System.out.println("---");

            flag = checkFromBegin(text, 0);
            if (!flag) {
                text.clear();
                continue;
            }
            //проверка на стыках
            for (int j = 0; j < length - 1; j++) {
                flag = checkFromTheEnd(text, columns[j].size() - 1);
                if (!flag) break;
            }
            if (!flag) {
                text.clear();
                continue;
            }
            flag = checkFromTheEnd(text, text.size() - 1);
            if (!flag) {
                text.clear();
                continue;
            } else break;
        }
        return text;
    }

    private static boolean checkFromBegin(List<Character> text, int pos) {
        int i = pos;
        char c;
        while (!Character.isLetter(c = text.get(i))) i++;
        StringBuilder sb = new StringBuilder();
        do {
            sb.append(c);
            i++;
        }while (Character.isLetter(c = text.get(i)));
        String s = sb.toString().toLowerCase();
        return Dictionary.exists(s);
    }

    private static boolean checkFromTheEnd(List<Character> text, int pos) {
        int i = pos;
        char c;
        if (!Character.isLetter(text.get(pos))) {
            if (i + 1 < text.size()) return checkFromBegin(text, i + 1);
            return true;
        }

        while (Character.isLetter(c = text.get(i))) i--;

        StringBuilder sb = new StringBuilder();
        i++;

        while (i < text.size() && Character.isLetter(c = text.get(i))) {
            sb.append(c);
            i++;
        }
        String s = sb.toString().toLowerCase();
        return Dictionary.exists(s);
    }
}
