package Decryptors.singlePermutation;

import java.util.List;

/**
 * @author Anna
 * 11-602
 */
public class SinglePermutationDecryptor {
    public static void main(String[] args) throws Exception{
        List<Character>[] columns = ToColumnsConverter.getColumns("src/Decryptors/singlePermutation/test.txt", 3);
        List<Character> text = Order.determineTheOrder(columns);
        text.forEach(i -> System.out.print(i));
        Output.write(text);
    }
}
