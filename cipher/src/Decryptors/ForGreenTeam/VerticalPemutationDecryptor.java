package Decryptors.ForGreenTeam;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

//special for green team:)

public class VerticalPemutationDecryptor {
    private static byte[][] keys;
    //мы не получили расшифрованный текст целиком. Мы получили только часть.
    public static void getTable(String file, byte[] key) {
        try {
            FileChannel fc = new FileInputStream(file).getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
            fc.read(byteBuffer);
            byteBuffer.rewind();
            CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
            int length = charBuffer.length();
            List<Character>[] table = new ArrayList[5];
            for (int i = 0; i < 5; i++) {
                ArrayList list = new ArrayList();

                //дальше хардкод. Значения получены путем нахождения
                // в тексте позиции, с которой каждая часть (part1.txt - part5.txt) начинается
                //это закомментировано в методе reverse (хз почему именно там, но когда мало времени, уже не особо задумываешься над структурой)
                int n;
                if (i == 0) n = 2630;
                else {
                    if (i == 1) n = 2628;
                    else {
                        if (i == 2) n = 2629;
                        else if (i == 3) n = 2634;
                        else n = 2631;
                    }
                }

                for (int j = 0; j < n; j++) {
                    if (charBuffer.hasRemaining()) list.add(charBuffer.get());
                }

                table[key[i]] = list;
            }

            int k = 0;
            CharBuffer out = CharBuffer.allocate(charBuffer.rewind().capacity());
            for (int i = 0; i < 2635; i++) {
                for (int j = 0; j < 5; j++) {
                    if (i < table[j].size()) {
                        System.out.print(table[j].get(i) + " ");
                        out.put(k, table[j].get(i));
                        k++;
                    }
                }
                System.out.println();
            }
            FileChannel fcOut = new FileOutputStream("src/Decryptors/ForGreenTeam/out.txt").getChannel();
            fcOut.write(Charset.forName("UTF-8").encode(out));

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {
        reverse("src/Decryptors/ForGreenTeam/GreenTeamN.txt", "src/Decryptors/ForGreenTeam/reverse.txt");
        byte[] key = {1,4,3,0,2};
        getTable("src/Decryptors/ForGreenTeam/reverse.txt", key);
    }

    public static void reverse(String file, String outputFile) {
        try {
            FileChannel fc = new FileInputStream(file).getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
            fc.read(byteBuffer);
            byteBuffer.rewind();
            CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
            StringBuilder stringBuilder = new StringBuilder();
            while (charBuffer.hasRemaining()) {
                stringBuilder.append(charBuffer.get());
            }
            //System.out.println(stringBuilder.length());
            //System.out.println(stringBuilder.indexOf("двсож"));
            //System.out.println(stringBuilder.indexOf("ссояо"));
            //System.out.println(stringBuilder.indexOf(".ийлт"));
            //System.out.println(stringBuilder.indexOf(" с  вУр,лоу"));
            stringBuilder.reverse();
            FileChannel out = new FileOutputStream(outputFile).getChannel();
            out.write(ByteBuffer.wrap(stringBuilder.toString().getBytes("UTF-8")));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        keys = new byte[120][5];
        int count = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                for (int k = 0; k < 5; k++) {
                    for (int l = 0; l < 5; l++) {
                        for (int m = 0; m < 5; m++) {
                            if (i != j && i != k && i!= l && i != m &&
                            j != k && j != l && j != m && k != l && k != m && l != m) {
                                keys[count][0] = (byte)i;
                                keys[count][1] = (byte)j;
                                keys[count][2] = (byte)k;
                                keys[count][3] = (byte)l;
                                keys[count][4] = (byte)m;
                                count++;
                            }
                        }
                    }
                }
            }
        }
    }
}
