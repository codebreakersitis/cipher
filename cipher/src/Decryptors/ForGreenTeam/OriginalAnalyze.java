package Decryptors.ForGreenTeam;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class OriginalAnalyze {
    public static void main(String[] args) throws Exception {
        //мы поняли суть шифра. Но не поняли, каким образом части (столбцы в шифровальной таблице)
        //следуют друг за другом. Чтобы понять, мы зашифровываем оригинал по частям. А потом ищем
        //совпадения в результате и в шифре зеленых (вот это вручную)

        VerticalPemutationDecryptor.reverse("src/Decryptors/ForGreenTeam/Onegin.txt", "src/Decryptors/ForGreenTeam/reverse.txt");
        FileChannel fc = new FileInputStream("src/Decryptors/ForGreenTeam/reverse.txt").getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int) fc.size());
        fc.read(byteBuffer);
        byteBuffer.rewind();
        CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
        int i = 0;
        int count = 0;
        System.out.println(charBuffer.length());

        StringBuilder[] builders = new StringBuilder[5];
        for (int j = 0; j < 5; j++) {
            builders[j] = new StringBuilder();
        }
        while (charBuffer.hasRemaining()) {
            builders[count].append(charBuffer.get());
            if (count < 4) count++;
            else count = 0;
        }
        FileChannel[] fileChannels = new FileChannel[5];
        fileChannels[0] = new FileOutputStream("src/Decryptors/ForGreenTeam/patr1.txt").getChannel();
        fileChannels[1] = new FileOutputStream("src/Decryptors/ForGreenTeam/patr2.txt").getChannel();
        fileChannels[2] = new FileOutputStream("src/Decryptors/ForGreenTeam/patr3.txt").getChannel();
        fileChannels[3] = new FileOutputStream("src/Decryptors/ForGreenTeam/patr4.txt").getChannel();
        fileChannels[4] = new FileOutputStream("src/Decryptors/ForGreenTeam/patr5.txt").getChannel();
        for (int j = 0; j < 5; j++) {
            fileChannels[j].write(ByteBuffer.wrap(builders[j].toString().getBytes("UTF-8")));
        }
    }
}
