package Decryptors.FrequencyAnalyze;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class OnlyLetters {
    public static void getOnlyLetters(String file, String output) throws  Exception{
        FileChannel fc = new FileInputStream(file).getChannel();
        ByteBuffer byteBuffer  = ByteBuffer.allocate((int)fc.size());
        fc.read(byteBuffer);
        byteBuffer.rewind();
        CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
        CharBuffer newCharBuffer = CharBuffer.allocate(charBuffer.length());
        int i = 0;
        while (charBuffer.hasRemaining()) {
            char c = charBuffer.get();
            if (c >= 'А' && c <= 'Я' || c >= 'а' && c <= 'я' || c >= '0' && c <= '9' || c == '\r' || c == '\n') {
                newCharBuffer.put(i, c);
            }
            else newCharBuffer.put(i, ' ');
            i++;
        }
        FileChannel out = new FileOutputStream(output).getChannel();
        newCharBuffer.rewind();
        out.write(Charset.forName("UTF-8").encode(newCharBuffer));
    }
}
