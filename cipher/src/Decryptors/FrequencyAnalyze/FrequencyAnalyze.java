package Decryptors.FrequencyAnalyze;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Dias
 * 11-602
 */
public class FrequencyAnalyze {
    public static void main(String[] args) throws IOException {
        FileChannel input = new FileInputStream("encryptedBook_by_RedTeam.txt").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int) input.size());
        input.read(buffer);
        String text = new String(buffer.array());
        Map<Character,Long> map_chastota = text.chars()
                .map(n->(char)(n))
                .mapToObj(n->(char)(n))
                .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));

        List<Long> list_of_values_text = getListValues(map_chastota);

        List<Character> list_map_chastota_keys = new ArrayList<>();

        list_of_values_text.forEach(n->list_map_chastota_keys.add(getKey(map_chastota,n)));

        Map<Character,Long> statistics = new HashMap<>();
        statistics.put('О',928L);
        statistics.put('А',866L);
        statistics.put('Е',810L);
        statistics.put('И',745L);
        statistics.put('Н',635L);
        statistics.put('Т',630L);
        statistics.put('Р',553L);
        statistics.put('С',545L);
        statistics.put('Л',432L);
        statistics.put('В',419L);
        statistics.put('К',347L);
        statistics.put('П',335L);
        statistics.put('М',329L);
        statistics.put('У',290L);
        statistics.put('Д',256L);
        statistics.put('Я',222L);
        statistics.put('Ы',211L);
        statistics.put('Ь',190L);
        statistics.put('З',181L);
        statistics.put('Б',151L);
        statistics.put('Г',141L);
        statistics.put('Й',131L);
        statistics.put('Ч',121L);
        statistics.put('Ю',103L);
        statistics.put('Х',92L);
        statistics.put('Ж',78L);
        statistics.put('Ш',77L);
        statistics.put('Ц',52L);
        statistics.put('Щ',49L);
        statistics.put('Ф',40L);
        statistics.put('Э',17L);
        statistics.put('Ъ',4L);

        List<Long> list_of_values_alhabet = getListValues(statistics);

        List<Character> res_list = new ArrayList<>();

        list_of_values_alhabet.forEach(x->res_list.add(getKey(statistics,x)));

        for (int i = 0; i<statistics.size();i++){
            text = text.replace(list_map_chastota_keys.get(i),res_list.get(i));
        }
        FileChannel output = new FileOutputStream("src/Decryptors/FrequencyAnalyze/outputDias.txt").getChannel();
        output.write(ByteBuffer.wrap(text.getBytes()));
    }
    public static List<Long> getListValues(Map<Character,Long> map){
        return map.values().stream()
                .sorted((s1,s3)-> (int)(s3-s1))
                .collect(Collectors.toList());
    }
    public static Character getKey(Map<Character,Long> map, long val){
        return map.entrySet().stream()
                .filter(s1 -> s1.getValue().equals(val))
                .map(Map.Entry::getKey)
                .findFirst()
                .get();
    }
}
