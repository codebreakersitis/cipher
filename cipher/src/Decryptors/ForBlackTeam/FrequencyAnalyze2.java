package Decryptors.ForBlackTeam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FrequencyAnalyze2 {
    public static Map<Character,Long> statistics = new HashMap<>();
    public static Map<Character, Long> frequency;
    public static Map<Character, Character> newValues;
    static {
        statistics.put('О',928L);
        statistics.put('А',866L);
        statistics.put('Е',810L);
        statistics.put('И',745L);
        statistics.put('Н',635L);
        statistics.put('Т',630L);
        statistics.put('Р',553L);
        statistics.put('С',545L);

    }
    public static void getFrequency(String file) throws Exception {
        FileChannel fc = new FileInputStream(file).getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
        fc.read(byteBuffer);
        byteBuffer.rewind();
        CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
        List<Character> characters = new ArrayList<>();
        while (charBuffer.hasRemaining()) {
            char c = charBuffer.get();
            if (Character.isLetter(c) || Character.isDigit(c)) {
                characters.add(c);
            }

        }
        frequency = characters.stream()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));
        List<Map.Entry<Character, Long>> frSet = frequency.entrySet().stream().sorted(new FrequencyComparator()) .collect(Collectors.toList());
        newValues = new HashMap<>();

        //можно проследить наш "путь исканий", закомментировав все последующие строки,
        // кроме первой, и запустив Main. Посмотреть на результат
        // И так каждый раз: раскомментировать ровно одну  строчку
        //и запустить Main снова:)
        newValues.put(frSet.get(0).getKey(), 'О');
        newValues.put('ж', 'К');
        newValues.put('д', 'Т');
        newValues.put('о', 'М');
        //newValues.put('х', 'Э');
        newValues.put('б', 'Н');
        newValues.put('я', 'И');
        newValues.put('ф', 'Г');
        newValues.put('ы', 'Е');
        newValues.put('п', 'Д');
        newValues.put('ч', 'У');
        newValues.put('т', 'Р');
        //newValues.put('6', 'С'); newValues.put('1', 'Ь');
        newValues.put('у', 'Ж');
        newValues.put('р', 'Х');
        newValues.put('л', 'В');
        newValues.put('с', 'Я');
        newValues.put('в', 'Б');
        newValues.put('6', 'Л'); newValues.put('щ', 'C');
        newValues.put('1', 'Ь');
        newValues.put('х', 'Ч');
        //даааааааа!!!! уже понятен текст!!! Это Одиссей!!!

        //остальные буквы понять не составляет труда, если взглянуть на output.txt
        newValues.put('е', 'П');
        newValues.put('0', 'Ы');
        newValues.put('ъ', 'З');
        newValues.put('а', 'Й');
        newValues.put('н', 'Ш');
        newValues.put('8', 'Щ');
        newValues.put('й', 'Ю');
        newValues.put('ц', 'Э');
        newValues.put('ь', 'Ц');
        newValues.put('7', 'Ъ');

    }

    public static class FrequencyComparator implements Comparator<Map.Entry<Character, Long>> {
        @Override
        public int compare(Map.Entry<Character, Long> o1, Map.Entry<Character, Long> o2) {
            return o2.getValue().compareTo(o1.getValue());
        }
    }

    public static void changeLetters(String file, String fileOut) throws Exception{
        FileChannel fc = new FileInputStream(file).getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
        fc.read(byteBuffer);
        byteBuffer.rewind();
        CharBuffer cb = Charset.forName("UTF-8").decode(byteBuffer);
        CharBuffer cb2 = CharBuffer.allocate(cb.length());
        StringBuilder stringBuilder = new StringBuilder();

        try {
            getFrequency(file);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        int i = 0;
        while (cb.hasRemaining()) {
            char c = cb.get();
            if (newValues.containsKey(c)) c = newValues.get(c);
            cb2.put(i, c);
            stringBuilder.append(c);
            i++;
        }
        List<String> words = mostFrequentWords(frequencyOfUse(Arrays.asList(stringBuilder.toString().split("[^А-Яа-я0-9]"))));
        words.forEach(System.out::println);
        FileChannel out =  new FileOutputStream(fileOut).getChannel();
        out.write(Charset.forName("UTF-8").encode(cb2));
    }

    public static List<String> mostFrequentWords(Map<String, Long> words) {
        return words.entrySet().stream()
                .filter(i -> i.getKey().length() > 2) //чтобы предлогов было меньше
                .filter(i -> i.getValue() > 2)
                .sorted(new Comparator2())
                .map(i -> i.getKey())
                .collect(Collectors.toList());
    }

    private static class Comparator2 implements Comparator<Map.Entry<String, Long>> {
        @Override
        public int compare(Map.Entry<String, Long> e1, Map.Entry<String, Long> e2) {
            return e2.getValue().compareTo(e1.getValue());
        }
    }

    public static Map<String, Long> frequencyOfUse(List<String> words) {
        return words.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }


}
