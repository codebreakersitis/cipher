package Decryptors.ForBlackTeam;

import Decryptors.FrequencyAnalyze.*;

public class Main {
    public static void main(String[] args) throws Exception{
        OnlyLetters.getOnlyLetters("src/Decryptors/ForBlackTeam/EncodedText_by_BlackTeam.txt",
                "src/Decryptors/ForBlackTeam/onlyLetters.txt");
        FrequencyAnalyze2.changeLetters("src/Decryptors/ForBlackTeam/onlyLetters.txt", "src/Decryptors/ForBlackTeam/output.txt");
    }
}
