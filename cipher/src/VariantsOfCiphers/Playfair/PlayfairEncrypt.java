package VariantsOfCiphers.Playfair;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * @author Anna
 * 11-602
 */
public class PlayfairEncrypt {
    private static char[][] table = new char[4][8];
    public static void encrypt(String file, String key) throws Exception{
        if (key.length() == 3) makeTable(key);
        FileChannel fc = new FileInputStream(file).getChannel();
        FileChannel out = new FileOutputStream("src/VariantsOfCiphers/Playfair/output.txt").getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
        fc.read(byteBuffer);
        byteBuffer.rewind();
        CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);

        int i = 0;
        int length = charBuffer.length();
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder1 = new StringBuilder();
        while (i < length) {
            char c;
            /*c = charBuffer.get();
            i++;
            while (i < length && !(c >= 'А' && c <= 'Я' || c >= 'а' && c <= 'я')) {
                stringBuilder.append(c);
                c = charBuffer.get();
                i++;
            }*/

            do {
                c = charBuffer.get();
                if (!(c >= 'А' && c <= 'Я' || c >= 'а' && c <= 'я')) stringBuilder.append(c);
                i++;
            } while (i < length && !(c >= 'А' && c <= 'Я' || c >= 'а' && c <= 'я'));

            //////работаем над парой
                char c1 = Character.toUpperCase(c);
                char c2 = 'Ф';
                int c1i = 0, c1j = 0, c2i = 0, c2j = 0;
                boolean flag = false;
                if (i < length)
                do {
                    c = charBuffer.get();
                    i++;
                    if (!(c >= 'А' && c <= 'Я' || c >= 'а' && c <= 'я')) {
                        flag = true;
                        stringBuilder1.append(c);
                    }
                    else c2 = Character.toUpperCase(c);
                }while (i < length && !(c >= 'А' && c <= 'Я' || c >= 'а' && c <= 'я'));


                for (int j = 0; j < 4; j++) {
                    for (int k = 0; k < 8; k++) {
                        if (table[j][k] == c1) {
                            c1i = j;
                            c1j = k;
                        }
                        if (table[j][k] == c2) {
                            c2i = j;
                            c2j = k;
                        }
                    }
                }

                if (c1i == c2i) {
                    if (c1j < 7) stringBuilder.append(table[c1i][c1j + 1]);
                    else stringBuilder.append(table[c1i][0]);
                    if (flag) {
                        stringBuilder.append(stringBuilder1);
                        stringBuilder1.delete(0, stringBuilder1.length());
                    }
                    if (c2j < 7) stringBuilder.append(table[c2i][c2j + 1]);
                    else stringBuilder.append(table[c2i][0]);
                }
                else {
                    if (c1j == c2j) {
                        if (c1i < 3) stringBuilder.append(table[c1i + 1][c1j]);
                        else stringBuilder.append(table[0][c1j]);
                        if (flag) {
                            stringBuilder.append(stringBuilder1);
                            stringBuilder1.delete(0, stringBuilder1.length());
                        }
                        if (c2i < 3) stringBuilder.append(table[c2i + 1][c2j]);
                        else stringBuilder.append(table[0][c2j]);
                    }
                    else {
                        stringBuilder.append(table[c1i][c2j]);
                        if (flag) {
                            stringBuilder.append(stringBuilder1);
                            stringBuilder1.delete(0, stringBuilder1.length());
                        }
                        stringBuilder.append(table[c2i][c1j]);
                    }
                }

            ////////////
        }

        charBuffer.clear();
        charBuffer = CharBuffer.wrap(stringBuilder.toString().toCharArray());
        byteBuffer.clear();
        byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
        out.write(byteBuffer);
    }

    public static void makeTable(String key) {
        key = key.toUpperCase();
        for (int i = 0; i < 3; i++) {
            table[0][i] = key.charAt(i);
        }
        char currentLetter = 'А';
        boolean flag = false;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                if (i != 0 || j >= 3) {
                    if (key.indexOf(currentLetter) == -1) {
                        table[i][j] = currentLetter;
                    }
                    else table[i][j] = ++currentLetter;
                    currentLetter++;
                }

            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) throws Exception{
        encrypt("temnye-allei-sbornik.txt", "рбх");
    }



}
