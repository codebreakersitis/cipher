/**
 * @author Elina
 * 11-602
 */
package VariantsOfCiphers.ElinaCipher;

public class Cipher2 {

    public static void main(String[] args) {
        String text = "Hello!";
        String key = "123456";
        Cipher2 m = new Cipher2();
        System.out.println(m.encrypt(text, key));
        System.out.println(m.decrypt(m.encrypt(text, key), key));
    }

    public String encrypt(String text, String keyWord) {
        byte[] arr = text.getBytes();
        byte[] keyarr = keyWord.getBytes();
        byte[] result = new byte[arr.length];
        for (int i = 0; i < arr.length; i++) {
            result[i] = (byte) (arr[i] ^ keyarr[i % keyarr.length]);
        }
        return new String(result);
    }

    public String decrypt(String text, String keyWord) {
        byte[] result = new byte[text.length()];
        byte[] keyarr = keyWord.getBytes();
        for (int i = 0; i < text.length(); i++) {
            result[i] = (byte) (text.charAt(i) ^ keyarr[i % keyarr.length]);
        }
        return new String(result);
    }
}
