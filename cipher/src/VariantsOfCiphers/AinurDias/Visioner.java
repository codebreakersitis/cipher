package VariantsOfCiphers.AinurDias;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dias
 * 11-602
 */
public class Visioner {
    public static void main(String[] args) throws Exception {
        FileChannel input = new FileInputStream("src/VariantsOfCiphers/AinurDias/test.txt").getChannel();
        FileChannel output = new FileOutputStream("src/VariantsOfCiphers/AinurDias/outputTest.txt").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int) input.size());
        input.read(buffer);
        String text = new String(buffer.array());
        // заполним таблицу виженер
        char[][] alphabet = new char[32][32];
        for (int i = 0; i < alphabet.length; i++) {
            for (int j = 0; j < alphabet[0].length; j++) {
                alphabet[i][j] = (char) ((i + j) % 32 + 'а');
            }
        }



        Map<Character, Integer> mapAlphabet = new HashMap<>();
        for (int i = 0; i < 32; i++) {
            mapAlphabet.put((char) ('а' + i), i);
        }

        //String text = "Я пошёл гулать, а потом случилось чтото такое ...";
        String key = "горка";
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            if ((text.charAt(i) >= 'а' && text.charAt(i) <= 'я') || (text.charAt(i) >= 'А' && text.charAt(i) <= 'Я')) {
                if ((text.charAt(i) >= 'А' && text.charAt(i) <= 'Я')) result.append(alphabet[mapAlphabet.get((char) (text.charAt(i)-'А'+'а'))][mapAlphabet.get(key.charAt(i % key.length()))]);
                else result.append(alphabet[mapAlphabet.get(text.charAt(i))][mapAlphabet.get(key.charAt(i % key.length()))]);
            } else result.append(text.charAt(i));
        }
        output.write(ByteBuffer.wrap(result.toString().getBytes()));
        System.out.println(result.toString());
    }
}
