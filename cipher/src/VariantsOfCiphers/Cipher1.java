package VariantsOfCiphers;

import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class Cipher1 {
    private static int key2 = 1000;

    public static char encode(char c) {
        return (char)((c -key2) * (c - key2) - 2 * (c - key2));
    }

    public static char decode(char c) {
        int res = key2 + 1 + (int)Math.sqrt((int) c + 1);
        return (char) res;
    }
}
