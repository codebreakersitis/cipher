import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
/**
 * @author Ainur
 * 11-602
 */
public class TeamWork {
    public static void main(String[] args) throws Exception {
        TeamWork project = new TeamWork();

        project.encode("temnye-allei-sbornik.txt", "горка");
        project.decode("output.txt", "горка");
    }

    public void encode(String path, String key) throws IOException {
        FileChannel input = null;
        try {input = new FileInputStream("temnye-allei-sbornik.txt").getChannel();} catch (FileNotFoundException e) {
            System.err.println("Файл не найден!!!");
        }
        FileChannel output = new FileOutputStream("output.txt").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int) input.size());
        input.read(buffer);

        String text = new String(buffer.array());

        char[][] alphabet = getVisiniorTable();

        Map<Character, Integer> mapAlphabet = getAlphabet();

        StringBuilder result = new StringBuilder();
        int k = 0;
        for (int i = 0; i < text.length(); i++) {
            if ((text.charAt(i) >= 'а' && text.charAt(i) <= 'я') || (text.charAt(i) >= 'А' && text.charAt(i) <= 'Я' || text.charAt(i) == 'Ё' || text.charAt(i) == 'ё')){
                if ((text.charAt(i) == 'Ё' || text.charAt(i) == 'ё')){
                    result.append(alphabet[mapAlphabet.get('е')][mapAlphabet.get(key.charAt((++k-1) % key.length()) )]);
                }
                else if ((text.charAt(i) >= 'А' && text.charAt(i) <= 'Я')){
                    result.append(alphabet[mapAlphabet.get((char) (text.charAt(i)-'А'+'а'))][mapAlphabet.get(key.charAt((++k-1) % key.length()))]);
                }
                else result.append(alphabet[mapAlphabet.get(text.charAt(i))][mapAlphabet.get(key.charAt((++k-1) % key.length()) )]);
            }
            else result.append(text.charAt(i));
        }
        output.write(ByteBuffer.wrap(result.toString().getBytes()));
        input.close();
        output.close();
    }

    public void decode(String inputPath, String key) throws IOException {
        FileChannel input = null;
        try {
            input = new FileInputStream(inputPath).getChannel();
        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден");
            e.printStackTrace();
        }
        FileChannel output = new FileOutputStream("outputTest.txt").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int) input.size());
        input.read(buffer);
        String text = new String(buffer.array());

        char[][] alphabet = getVisiniorTable();

        Map<Character, Integer> mapAlphabet = getAlphabet();

        StringBuilder result = new StringBuilder();
        int k = 0;
        for (int i = 0; i < text.length(); i++) {
            if ((text.charAt(i) >= 'а' && text.charAt(i) <= 'я') || (text.charAt(i) >= 'А' && text.charAt(i) <= 'Я')) {

                int line = mapAlphabet.get(key.charAt((++k-1) % key.length()));  // находим позицую строки (по ключу)

                for (int col = 0; col < mapAlphabet.size(); col++) { // находим позицую столбца (по слову текста)
                    if (text.charAt(i) == alphabet[line][col]) result.append((char) (col + 'а'));
                }
            }
            else result.append(text.charAt(i));
        }
        output.write(ByteBuffer.wrap(result.toString().getBytes()));

        input.close();
        output.close();
    }

    public static char[][] getVisiniorTable(){ // заполним таблицу виженер
        char[][] alphabet = new char[32][32];
        for (int i = 0; i < alphabet.length; i++) {
            for (int j = 0; j < alphabet[0].length; j++) {
                alphabet[i][j] = (char) ((i + j) % 32 + 'а');
            }
        }
        return alphabet;
    }

    public static Map<Character, Integer> getAlphabet(){  // вернем алфавит
        Map<Character, Integer> mapAlphabet = new HashMap<>();
        for (int i = 0; i < 32; i++) {
            mapAlphabet.put((char) ('а' + i), i);
        }
        return mapAlphabet;
    }
}
