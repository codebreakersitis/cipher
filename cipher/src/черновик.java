import VariantsOfCiphers.Cipher1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;

public class черновик {
    private static List<Character> punctuation = new ArrayList<>();
    static  {
        punctuation.add(' ');
        punctuation.add('.');
        punctuation.add(',');
        punctuation.add('!');
        punctuation.add(';');
        punctuation.add(':');
        punctuation.add('?');
        punctuation.add('-');
        punctuation.add('\000');
        punctuation.add('\r');
        punctuation.add('\n');
        for (int i = '0'; i <= '9'; i++) {
            punctuation.add((char) i);
        }
        for (int i = 'A'; i <= 'z'; i++) {
            punctuation.add((char) i);
        }
    }

    public static void encode() throws Exception {
        FileChannel fc = new FileInputStream("test.txt").getChannel();
        FileChannel out = new FileOutputStream("outputTest.txt").getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
        CharBuffer charBuffer = CharBuffer.allocate((int)fc.size());
        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

        fc.read(byteBuffer);
        byteBuffer.flip();
        charBuffer.clear();
        charBuffer = decoder.decode(byteBuffer);

        CharBuffer newChars = CharBuffer.allocate(charBuffer.capacity());

        int n = charBuffer.length();

        for (int i = 0; i < n; i++){
            char c = charBuffer.get();
            if (Character.isLetter(c) && !punctuation.contains(c)) {
                newChars.put(i, Cipher1.encode(c));
            }
            else newChars.put(i, c);
        }
        out.write(encoder.encode(newChars));

        fc.close();
    }

    public static void decode() throws Exception {
        FileChannel fc = new FileInputStream("outputTest.txt").getChannel();
        FileChannel out = new FileOutputStream("test.txt").getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
        CharBuffer charBuffer = CharBuffer.allocate((int)fc.size());

        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

        fc.read(byteBuffer);
        byteBuffer.flip();
        charBuffer.clear();
        charBuffer = decoder.decode(byteBuffer);

        CharBuffer newChars = CharBuffer.allocate(charBuffer.capacity());

        int n = charBuffer.length();

        for (int i = 0; i < n; i++){
            char c = charBuffer.get();
            if (!punctuation.contains(c)) {
                newChars.put(i, Cipher1.decode(c));
            }
            else newChars.put(i, c);
        }
        out.write(encoder.encode(newChars));

        fc.close();
    }

    public static void main(String[] args) throws Exception{
        encode();
        decode();
    }
}
