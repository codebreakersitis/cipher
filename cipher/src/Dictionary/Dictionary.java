package Dictionary;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anna
 * 11-602
 */
public class Dictionary {
    public static List<String> words = new ArrayList<>();

    static {
        try {
            FileChannel in = new FileInputStream(new File("litf-win.txt")).getChannel();
            MappedByteBuffer map = in.map(FileChannel.MapMode.READ_ONLY, 0, in.size());
            List<ByteBuffer> buffers = new ArrayList<>();
            int size = map.capacity();

            while (map.hasRemaining()) {
                ByteBuffer b = ByteBuffer.allocate(size);
                for (int i = 0; i < size; i++) {
                    byte k = map.get();
                    b.put(i, k);
                }
                buffers.add(b);
            }

            List<CharBuffer> charBuffers = new ArrayList<>();
            CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();

            buffers.stream().forEach(i -> {
                try {
                    charBuffers.add(decoder.decode(i));
                }
                catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            });

            charBuffers.stream().forEach(b -> {
                StringBuilder builder = new StringBuilder();
                do {
                    char c = b.get();
                    while (Character.isLetter(c)) {
                        builder.append(c);
                        c = b.get();
                    }
                    if (builder.length() > 0) {
                        words.add(builder.toString());
                        builder.delete(0, builder.length());
                    }

                } while (b.hasRemaining());

            });

            //words.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean exists(String s) {
        return words.contains(s);
    }

    //test
    public static void main(String[] args) {
        System.out.println(exists("дом"));
        System.out.println(exists("дома"));
        System.out.println(exists("доме"));
        System.out.println(exists("домишко"));
        System.out.println(exists("домишке"));
    }
}

