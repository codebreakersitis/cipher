package Dictionary;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * @author Anna
 * 11-602
 */

public class CheckingByDictionary {
    public static boolean check(String file) {
        int count = 0;
        try {
            FileChannel fc = new FileInputStream(file).getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate((int)fc.size());
            fc.read(byteBuffer);
            byteBuffer.rewind();
            CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
            StringBuilder stringBuilder = new StringBuilder(charBuffer);
            //String part = stringBuilder.substring(0, 200);

            String[] words = stringBuilder.toString().split("[^А-Яа-я]");
            for (int i = 0; i < words.length && count < 10; i++) {
                if (words[i].length() > 2 && Dictionary.exists(words[i].toLowerCase())) count++;
            }
        }
        catch (Exception e) {
            return false;
        }
        return count >= 10;
    }
}
